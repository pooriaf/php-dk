#  DigiKala Task
Author: Pooria Farhad

### Prerequisites
* Install Symfony framework requirements like WebServer and MySQL, PHP >= 7.1.3  
* Install Redis Server and ElasticSearch

### How to run
* Run `composer install` to install dependencies
* Create a mysql database and update env file
* Bootstrap application `php bin/console app-bootstrapper`
* Open root of the project in a browser 

##### Feel free to ask any question
pooriaf@gmail.com