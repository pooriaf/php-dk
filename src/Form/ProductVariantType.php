<?php

namespace App\Form;

use App\Entity\ProductVariant;
use App\Entity\Variant;
use App\Entity\VariantValue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductVariantType
 * @package App\Form
 */
class ProductVariantType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('sku')
            ->add('price');

        // Dynamically Load Variants
        foreach ($options['variants'] as $variant) {
            $data = null;
            $choices = [];
            foreach ($variant->getVariantValues() as $variantValue) {
                $choices[$variantValue->getValue()] = $variantValue->getId();
                foreach ($options['product_items'] as $productItem) {
                    if ($productItem->getVariantValue()->getId() === $variantValue->getId())
                        $data = $variantValue->getId();
                }
            }
            $builder->add($variant->getName(), ChoiceType::class, [
                'choices' => $choices,
                'mapped' => false,
                'data' => $data,
            ]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('variants');
        $resolver->setRequired('product_items');

        $resolver->setDefaults([
            'data_class' => ProductVariant::class,
        ]);
    }
}
