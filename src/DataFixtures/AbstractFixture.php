<?php
/**
 * Created by PhpStorm.
 * User: pooriafarhad
 * Date: 12/2/18
 * Time: 6:12 PM
 */

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class AbstractFixture
 * It holds the base requirements of fixtures
 * @package App\DataFixtures
 */
abstract class AbstractFixture extends Fixture
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * AbstractFixture constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }
}