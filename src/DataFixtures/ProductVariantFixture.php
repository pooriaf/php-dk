<?php

namespace App\DataFixtures;

use App\Entity\ProductVariant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ProductVariantFixture
 * @package App\DataFixtures
 */
class ProductVariantFixture extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $productVariant = new ProductVariant();
        $productVariant->setName($this->faker->word);
        $productVariant->setPrice($this->faker->numberBetween(10000, 999999));
        $productVariant->setSku($this->faker->bothify('???##'));
        $productVariant->setProduct($this->getReference('product'));

        $manager->persist($productVariant);

        $manager->flush();

        $this->setReference('product_variant', $productVariant);
    }


    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
