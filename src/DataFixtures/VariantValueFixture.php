<?php

namespace App\DataFixtures;

use App\Entity\VariantValue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class VariantValueFixture
 * @package App\DataFixtures
 */
class VariantValueFixture extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $variantValue = new VariantValue();
        $variantValue->setValue('Red');
        $variantValue->setVariant($this->getReference('variant'));
        $manager->persist($variantValue);
        $manager->flush();

        $variantValue = new VariantValue();
        $variantValue->setValue('Blue');
        $variantValue->setVariant($this->getReference('variant'));
        $manager->persist($variantValue);
        $manager->flush();

        $variantValue = new VariantValue();
        $variantValue->setValue('Green');
        $variantValue->setVariant($this->getReference('variant'));
        $manager->persist($variantValue);
        $manager->flush();

        $variantValue = new VariantValue();
        $variantValue->setValue('White');
        $variantValue->setVariant($this->getReference('variant'));
        $manager->persist($variantValue);
        $manager->flush();

        $variantValue = new VariantValue();
        $variantValue->setValue('Black');
        $variantValue->setVariant($this->getReference('variant'));
        $manager->persist($variantValue);
        $manager->flush();

        $this->setReference('variant_value', $variantValue);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }
}
