<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ProductFixture
 * @package App\DataFixtures
 */
class ProductFixture extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
         $product = new Product();
         $product->setTitle($this->faker->word);
         $product->setDescription($this->faker->text);
         $product->setUser($this->getReference('user'));
         $manager->persist($product);

        $manager->flush();

        $this->setReference('product', $product);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
