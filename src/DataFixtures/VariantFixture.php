<?php

namespace App\DataFixtures;

use App\Entity\Variant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class VariantFixture
 * @package App\DataFixtures
 */
class VariantFixture extends Fixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $variant = new Variant();
        $variant->setName('Color');
        $manager->persist($variant);

        $manager->flush();

        $this->setReference('variant', $variant);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}
