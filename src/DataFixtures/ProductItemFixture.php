<?php

namespace App\DataFixtures;

use App\Entity\ProductItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ProductItemFixture
 * @package App\DataFixtures
 */
class ProductItemFixture extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $productVariant = new ProductItem();
        $productVariant->setVariantValue($this->getReference('variant_value'));
        $productVariant->setProductVariant($this->getReference('product_variant'));

        $manager->persist($productVariant);

        $manager->flush();

    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 5;
    }
}
