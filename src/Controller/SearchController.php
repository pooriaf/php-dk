<?php

namespace App\Controller;

use App\Entity\Variant;
use App\Service\ElasticManager\Search;
use Doctrine\Common\Cache\MemcachedCache;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Search Action Handler
 * @package App\Controller
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/dashboard/search", name="product_search", methods="GET")
     * @param Request $request
     * @param Search $search
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function index(Request $request, Search $search)
    {
        // Populate search manager class to handle the search
        $requestValues = [];
        $requestValues['priceFrom'] = $request->get('price-from');
        $search->setPriceFrom($requestValues['priceFrom']);
        $requestValues['priceTo'] = $request->get('price-to');
        $search->setPriceTo($requestValues['priceTo']);
        $requestValues['string'] = $request->get('string');
        $search->setString($requestValues['string']);
        $requestValues['colors'] = $request->get('colors');
        $search->setColors($requestValues['colors']);

        // Getting user queries as a simple string (it's better to exclude irrelevant keys)
        $querySting = $request->server->get('QUERY_STRING');
        $cacheKey = 'PRODUCT.' . $querySting;


        // To use memcached, just change the adapter
        // $appCache = new MemcachedCache();

        $appCache = new RedisCache(new Client());
        if ($appCache->has($cacheKey)) {
            $products = $appCache->get($cacheKey);
        } else {
            $products = $search->process();
            $appCache->set($cacheKey, $products, 600);
        }

        // Get Variants, at the moment there is only color but It is possible to have dynamic variants based on the schema.
        $variant = ($this->getDoctrine()->getRepository(Variant::class))->findOneBy(['name' => 'Color']);

        return $this->render('search/index.html.twig', [
            'request_values' => $requestValues,
            'products' => $products,
            'color_values' => $variant->getVariantValues(),
        ]);
    }
}
