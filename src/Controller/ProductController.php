<?php

namespace App\Controller;

use App\Entity\Product;
use App\Event\ProductManipulatedEvent;
use App\Event\ProductRemovedEvent;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Product Management
 * @Route("dashboard/products")
 * @package App\Controller
 */
class ProductController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ProductController constructor.
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/", name="product_index", methods="GET")
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function index(ProductRepository $productRepository): Response
    {
        // It passes only logged in user's products
        return $this->render('product/index.html.twig', ['products' => $productRepository->findBy(['user' => $this->getUser()])]);
    }

    /**
     * @Route("/new", name="product_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            // Dispatch the event
            $event = new ProductManipulatedEvent($product->getId());
            $this->eventDispatcher->dispatch(ProductManipulatedEvent::NAME, $event);

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods="GET|POST")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function edit(Request $request, Product $product): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $product);

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // Dispatch the event
            $event = new ProductManipulatedEvent($product->getId());
            $this->eventDispatcher->dispatch(ProductManipulatedEvent::NAME, $event);

            return $this->redirectToRoute('product_index', ['id' => $product->getId()]);
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_delete", methods="DELETE")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete' . $product->getId(), $request->request->get('_token'))) {
            $this->denyAccessUnlessGranted('DELETE', $product);

            $productId = $product->getId();
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            // Dispatch the event
            $event = new ProductManipulatedEvent($productId);
            $this->eventDispatcher->dispatch(ProductRemovedEvent::NAME, $event);
        }

        return $this->redirectToRoute('product_index');
    }
}
