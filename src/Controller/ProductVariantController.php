<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductItem;
use App\Entity\ProductVariant;
use App\Entity\Variant;
use App\Entity\VariantValue;
use App\Event\ProductManipulatedEvent;
use App\Form\ProductVariantType;
use App\Repository\ProductRepository;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Product Variant Management
 * @Route("dashboard/products/{product}/variants")
 * @package App\Controller
 */
class ProductVariantController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ProductVariantController constructor.
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @Route("/", name="product_variant_index", methods="GET")
     * @param $product
     * @param ProductRepository $productRepository
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index($product, ProductRepository $productRepository): Response
    {
        // Get user products
        $product = $productRepository->findOneByUser($product, $this->getUser()->getId());
        return $this->render('product_variant/index.html.twig', ['product' => $product, 'product_variants' => $product->getProductVariants()]);
    }

    /**
     * @Route("/new", name="product_variant_new", methods="GET|POST")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function new(Product $product, Request $request): Response
    {
        $this->denyAccessUnlessGranted('CREATE_VARIANT', $product);

        $productVariant = new ProductVariant();
        if ($request->isMethod('GET')) {
            $faker = Factory::create();
            $productVariant->setSku($faker->bothify('???##'));
        }
        $form = $this->createForm(ProductVariantType::class, $productVariant, [
            'variants' => ($this->getDoctrine()->getRepository(Variant::class))->findAll(),
            'product_items' => []
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $productVariant->setProduct($product);
            $em->persist($productVariant);

            $productItem = new ProductItem();
            $productItem->setVariantValue(($this->getDoctrine()->getRepository(VariantValue::class))->find($form->get('Color')->getNormData()));
            $productVariant->addProductItem($productItem);
            $em->persist($productItem);
            $em->flush();

            // Dispatch the event
            $event = new ProductManipulatedEvent($product->getId());
            $this->eventDispatcher->dispatch(ProductManipulatedEvent::NAME, $event);

            return $this->redirectToRoute('product_variant_index', ['product' => $product->getId()]);
        }

        return $this->render('product_variant/new.html.twig', [
            'product' => $product,
            'product_variant' => $productVariant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_variant_edit", methods="GET|POST")
     * @param Product $product
     * @param ProductVariant $productVariant
     * @param Request $request
     * @return Response
     */
    public function edit(Product $product, ProductVariant $productVariant, Request $request): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $productVariant);

        $form = $this->createForm(ProductVariantType::class, $productVariant, [
            'variants' => ($this->getDoctrine()->getRepository(Variant::class))->findAll(),
            'product_items' => $productVariant->getProductItems()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $variantValue = $form->get('Color')->getNormData();
            ($this->getDoctrine()->getRepository(ProductItem::class))->clearProductItems($productVariant->getId());
            $productItem = new ProductItem();
            $productItem->setVariantValue(($this->getDoctrine()->getRepository(VariantValue::class))->find($variantValue));

            $productVariant->addProductItem($productItem);
            $em->persist($productItem);

            $em->flush();

            // Dispatch the event
            $event = new ProductManipulatedEvent($product->getId());
            $this->eventDispatcher->dispatch(ProductManipulatedEvent::NAME, $event);

            return $this->redirectToRoute('product_variant_index', ['product' => $product->getId()]);
        }

        return $this->render('product_variant/edit.html.twig', [
            'product' => $product,
            'product_variant' => $productVariant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="product_variant_delete", methods="DELETE")
     * @param Request $request
     * @param ProductVariant $productVariant
     * @return Response
     */
    public function delete(Request $request, ProductVariant $productVariant): Response
    {
        if ($this->isCsrfTokenValid('delete' . $productVariant->getId(), $request->request->get('_token'))) {
            $this->denyAccessUnlessGranted('DELETE', $productVariant);
            $em = $this->getDoctrine()->getManager();
            $em->remove($productVariant);
            $em->flush();

            // Dispatch the event
            $event = new ProductManipulatedEvent($productVariant->getProduct()->getId());
            $this->eventDispatcher->dispatch(ProductManipulatedEvent::NAME, $event);
        }

        return $this->redirectToRoute('product_variant_index', ['product' => $productVariant->getProduct()->getId()]);
    }
}
