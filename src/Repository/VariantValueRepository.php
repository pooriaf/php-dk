<?php

namespace App\Repository;

use App\Entity\VariantValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VariantValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method VariantValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method VariantValue[]    findAll()
 * @method VariantValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariantValueRepository extends ServiceEntityRepository
{
    /**
     * VariantValueRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VariantValue::class);
    }
}
