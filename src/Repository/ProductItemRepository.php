<?php

namespace App\Repository;

use App\Entity\ProductItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductItem[]    findAll()
 * @method ProductItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductItemRepository extends ServiceEntityRepository
{
    /**
     * ProductItemRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductItem::class);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function clearProductItems($product)
    {
        return $this->createQueryBuilder('pi')
            ->andWhere('pi.product_variant = :val')
            ->setParameter('val', $product)
            ->delete()
            ->getQuery()
            ->execute();
    }

}
