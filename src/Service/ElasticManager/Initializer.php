<?php

namespace App\Service\ElasticManager;


/**
 * Class Initializer
 * @package App\Service\ElasticManager
 */
class Initializer extends ElasticAbstract
{
    /**
     * ElasticSearch Shards Count
     */
    CONST SHARD_COUNT = 1;
    /**
     * ElasticSearch Replicas Count
     */
    CONST REPLICA_COUNT = 1;

    /**
     * @return array
     */
    public function generateBaseConfig()
    {
        $params = [
            'index' => $this::INDEX,
            'body' => [
                'settings' => [
                    'number_of_shards' => $this::SHARD_COUNT,
                    'number_of_replicas' => $this::REPLICA_COUNT
                ],
                'mappings' => [
                    $this::TYPE => [
                        'properties' => [
                            'id' => ['type' => 'integer'],
                            'title' => ['type' => 'text'],
                            'description' => ['type' => 'text'],
                            'variants' => [
                                'type' => 'nested',
                                'properties' => [
                                    'id' => ['type' => 'integer'],
                                    'name' => ['type' => 'text'],
                                    'sku' => ['type' => 'keyword'],
                                    'price' => ['type' => 'long'],
                                    'color' => ['type' => 'keyword']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $this->client->indices()->create($params);
    }
}