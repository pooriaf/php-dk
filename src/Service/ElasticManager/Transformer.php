<?php

namespace App\Service\ElasticManager;


use App\Repository\ProductRepository;

/**
 * Transform structure to apply on Elastic
 * @package App\Service\ElasticManager
 */
class Transformer
{
    /**
     * @var
     */
    private $product;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Transformer constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @return array
     */
    public function handle()
    {
        $product = $this->productRepository->find($this->product);
        $productVariants = [];
        foreach ($product->getProductVariants() as $productVariant) {
            $productVariants[] = [
                'id' => $productVariant->getId(),
                'name' => $productVariant->getName(),
                'sku' => $productVariant->getSku(),
                'price' => $productVariant->getPrice(),
                'color' => ($productVariant->getProductItems()->last())->getVariantValue()->getValue()
            ];
        }
        $productStructure = [
            'id' => $product->getId(),
            'title' => $product->getTitle(),
            'description' => $product->getDescription(),
            'variants' => $productVariants
        ];

        return $productStructure;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): void
    {
        $this->product = $product;
    }
}