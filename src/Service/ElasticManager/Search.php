<?php

namespace App\Service\ElasticManager;


/**
 * Class Search
 * @package App\Service\ElasticManager
 */
class Search extends ElasticAbstract
{
    /**
     * @var
     */
    private $priceFrom;
    /**
     * @var
     */
    private $priceTo;
    /**
     * @var
     */
    private $colors;
    /**
     * @var
     */
    private $string;

    /**
     * @return array
     */
    public function process()
    {

        // Initial Parameters
        $params = [
            'index' => $this::INDEX,
            'type' => $this::TYPE
        ];

        // If the request has a title to search then it tries to find match items based on both title and description of products
        if ($this->string) {
            $params['body']['query']['bool']['minimum_should_match'] = 1;
            // based on title
            $params['body']['query']['bool']['should'][] = [
                'match' => [
                    'title' =>
                        [
                            'query' => $this->string,
                            'boost' => 2
                        ]
                ]
            ];
            // based on description
            $params['body']['query']['bool']['should'][] = [
                'match' => [
                    'description' =>
                        [
                            'query' => $this->string,
                            'boost' => 1
                        ]
                ]
            ];
        }
        // set price range
        if ($this->priceFrom || $this->priceTo) {
            $values = [];
            if ($this->priceFrom)
                $values['gte'] = $this->priceFrom;
            if ($this->priceTo)
                $values['lte'] = $this->priceTo;

            $params['body']['query']['bool']['filter'][] = [
                'nested' => [
                    'path' => 'variants',
                    'query' =>
                        [
                            'range' => [
                                'variants.price' => $values
                            ],
                        ]
                ]
            ];
        }

        // set colors as a array to match
        if ($this->colors) {
            $params['body']['query']['bool']['filter'][] = [
                'nested' => [
                    'path' => 'variants',
                    'query' =>
                        [
                            'terms' => ['variants.color' => $this->colors],
                        ]
                ]
            ];

        }


        return $this->client->search($params);
    }

    /**
     * @param mixed $colors
     */
    public function setColors($colors): void
    {
        $this->colors = $colors;
    }

    /**
     * @param mixed $priceTo
     */
    public function setPriceTo($priceTo): void
    {
        $this->priceTo = $priceTo;
    }

    /**
     * @param mixed $priceFrom
     */
    public function setPriceFrom($priceFrom): void
    {
        $this->priceFrom = $priceFrom;
    }

    /**
     * @param mixed $string
     */
    public function setString($string): void
    {
        $this->string = $string;
    }
}