<?php

namespace App\Service\ElasticManager;


/**
 * Class Synchronizer
 * @package App\Service\ElasticManager
 */
class Synchronizer extends ElasticAbstract
{
    /**
     * @param array $data
     * @return array
     */
    public function index(array $data)
    {
        $params = [
            'index' => $this::INDEX,
            'type' => $this::TYPE,
            'id' => $data['id'],
            'body' => $data
        ];

        return $this->client->index($params);
    }

    /**
     * @param $id
     * @return array
     */
    public function remove($id)
    {
        $params = [
            'index' => $this::INDEX,
            'type' => $this::TYPE,
            'id' => $id
        ];

        return $this->client->delete($params);
    }

    /**
     * @param $id
     * @return array
     */
    public function get($id)
    {
        $params = [
            'index' => $this::INDEX,
            'type' => $this::TYPE,
            'id' => $id
        ];

        return $this->client->get($params);
    }

    /**
     * @return array
     */
    public function search()
    {
        $params = [
            'index' => $this::INDEX,
            'type' => $this::TYPE,
            'body' => [
                'query' => [
                    'match' => [
                        'testField' => 'abc'
                    ]
                ]
            ]
        ];

        return $this->client->search($params);
    }
}