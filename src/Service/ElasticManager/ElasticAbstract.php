<?php
/**
 * Created by PhpStorm.
 * User: pooriafarhad
 * Date: 12/6/18
 * Time: 7:56 PM
 */

namespace App\Service\ElasticManager;


use Elasticsearch\ClientBuilder;

/**
 * Class ElasticAbstract
 * @package App\Service\ElasticManager
 */
class ElasticAbstract
{
    /**
     * ElasticSearch Index Name
     */
    CONST INDEX = 'market';
    /**
     * ElasticSearch Type Name
     */
    CONST TYPE = 'products';

    /**
     * @var \Elasticsearch\Client
     */
    protected $client;

    /**
     * ElasticAbstract constructor.
     */
    public function __construct()
    {
        $this->client = ClientBuilder::create()->build();
    }
}