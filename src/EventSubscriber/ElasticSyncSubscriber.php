<?php

namespace App\EventSubscriber;

use App\Event\ProductManipulatedEvent;
use App\Event\ProductRemovedEvent;
use App\Repository\ProductRepository;
use App\Service\ElasticManager\Synchronizer;
use App\Service\ElasticManager\Transformer;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ElasticSyncSubscriber
 * @package App\EventSubscriber
 */
class ElasticSyncSubscriber implements EventSubscriberInterface
{
    /**
     * @var Synchronizer
     */
    private $elasticSynchronizer;
    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * ElasticSyncSubscriber constructor.
     * @param Synchronizer $elasticSynchronizer
     * @param Transformer $transformer
     */
    public function __construct(Synchronizer $elasticSynchronizer, Transformer $transformer)
    {
        $this->elasticSynchronizer = $elasticSynchronizer;
        $this->transformer = $transformer;
    }

    /**
     * @param $event
     */
    public function onProductManipulated($event)
    {
        $this->transformer->setProduct($event->getProduct());
        $structuredProduct = $this->transformer->handle();
        try {
            $this->elasticSynchronizer->remove($event->getProduct());
            $this->elasticSynchronizer->index($structuredProduct);
        } catch (Missing404Exception $e) {
            $this->elasticSynchronizer->index($structuredProduct);
        }
    }

    /**
     * @param $event
     */
    public function onProductRemoved($event)
    {
        $this->elasticSynchronizer->remove($event->getProduct());
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ProductManipulatedEvent::NAME => 'onProductManipulated',
            ProductRemovedEvent::NAME => 'onProductRemoved'
        ];
    }
}
