<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductItemRepository")
 * @ORM\Table(name="product_items")
 */
class ProductItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductVariant", inversedBy="productItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product_variant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VariantValue", inversedBy="productItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $variant_value;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ProductVariant|null
     */
    public function getProductVariant(): ?ProductVariant
    {
        return $this->product_variant;
    }

    /**
     * @param ProductVariant|null $product_variant
     * @return ProductItem
     */
    public function setProductVariant(?ProductVariant $product_variant): self
    {
        $this->product_variant = $product_variant;

        return $this;
    }

    /**
     * @return VariantValue|null
     */
    public function getVariantValue(): ?VariantValue
    {
        return $this->variant_value;
    }

    /**
     * @param VariantValue|null $variant_value
     * @return ProductItem
     */
    public function setVariantValue(?VariantValue $variant_value): self
    {
        $this->variant_value = $variant_value;

        return $this;
    }
}
