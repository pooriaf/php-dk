<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VariantValueRepository")
 * @ORM\Table(name="variant_values")
 */
class VariantValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Variant", inversedBy="variantValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $variant;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductItem", mappedBy="variant_value")
     */
    private $productItems;

    /**
     * VariantValue constructor.
     */
    public function __construct()
    {
        $this->productItems = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return VariantValue
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return variant|null
     */
    public function getVariant(): ?variant
    {
        return $this->variant;
    }

    /**
     * @param variant|null $variant
     * @return VariantValue
     */
    public function setVariant(?variant $variant): self
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * @return Collection|ProductItem[]
     */
    public function getProductItems(): Collection
    {
        return $this->productItems;
    }

    /**
     * @param ProductItem $productItem
     * @return VariantValue
     */
    public function addProductItem(ProductItem $productItem): self
    {
        if (!$this->productItems->contains($productItem)) {
            $this->productItems[] = $productItem;
            $productItem->setVariantValue($this);
        }

        return $this;
    }

    /**
     * @param ProductItem $productItem
     * @return VariantValue
     */
    public function removeProductItem(ProductItem $productItem): self
    {
        if ($this->productItems->contains($productItem)) {
            $this->productItems->removeElement($productItem);
            // set the owning side to null (unless already changed)
            if ($productItem->getVariantValue() === $this) {
                $productItem->setVariantValue(null);
            }
        }

        return $this;
    }
}
