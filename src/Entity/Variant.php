<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VariantRepository")
 * @ORM\Table(name="variants")
 */
class Variant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VariantValue", mappedBy="variant")
     */
    private $variantValues;

    /**
     * Variant constructor.
     */
    public function __construct()
    {
        $this->variantValues = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Variant
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|VariantValue[]
     */
    public function getVariantValues(): Collection
    {
        return $this->variantValues;
    }

    /**
     * @param VariantValue $variantValue
     * @return Variant
     */
    public function addVariantValue(VariantValue $variantValue): self
    {
        if (!$this->variantValues->contains($variantValue)) {
            $this->variantValues[] = $variantValue;
            $variantValue->setVariant($this);
        }

        return $this;
    }

    /**
     * @param VariantValue $variantValue
     * @return Variant
     */
    public function removeVariantValue(VariantValue $variantValue): self
    {
        if ($this->variantValues->contains($variantValue)) {
            $this->variantValues->removeElement($variantValue);
            // set the owning side to null (unless already changed)
            if ($variantValue->getVariant() === $this) {
                $variantValue->setVariant(null);
            }
        }

        return $this;
    }
}
