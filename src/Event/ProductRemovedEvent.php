<?php
namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class ProductRemovedEvent
 * @package App\Event
 */
class ProductRemovedEvent extends Event
{
    /**
     * Event Name
     */
    const NAME = 'product.removed';
    /**
     * @var
     */
    protected $product;

    /**
     * @param $productId
     */
    public function __construct($productId)
    {
        $this->product = $productId;
    }

    /**
     * @return int
     */
    public function getProduct(): int
    {
        return $this->product;
    }
}