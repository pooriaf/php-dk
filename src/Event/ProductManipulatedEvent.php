<?php
namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class ProductManipulatedEvent
 * @package App\Event
 */
class ProductManipulatedEvent extends Event
{
    /**
     * Event Name
     */
    const NAME = 'product.manipulated';

    /**
     * @var
     */
    protected $product;

    /**
     * @param $productId
     */
    public function __construct($productId)
    {
        $this->product = $productId;
    }

    /**
     * @return int
     */
    public function getProduct(): int
    {
        return $this->product;
    }
}