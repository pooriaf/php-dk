<?php

namespace App\Command;

use App\Entity\Product;
use App\Service\ElasticManager\Initializer;
use App\Service\ElasticManager\Synchronizer;
use App\Service\ElasticManager\Transformer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ElasticBootstrapperCommand
 * @package App\Command
 */
class AppBootstrapperCommand extends Command
{
    private $container;

    /**
     * @var string
     */
    protected static $defaultName = 'app-bootstrapper';


    public function __construct(?string $name = null, ContainerInterface $container)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setDescription('To initialize the index');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arrayInput = new ArrayInput([]);
        $command = $this->getApplication()->find('doctrine:migrations:migrate');
        $command->run($arrayInput, $output);

        $command = $this->getApplication()->find('doctrine:fixtures:load');
        $command->run($arrayInput, $output);

        $elasticInitializer = new Initializer();
        $elasticInitializer->generateBaseConfig();
        $elasticSynchronizer = new Synchronizer();

        $productRepository = $this->container->get('doctrine')->getRepository(Product::class);
        $elasticTransformer = new Transformer($productRepository);
        $defaultProducts = $productRepository->findAll();
        foreach ($defaultProducts as $defaultProduct) {
            $elasticTransformer->setProduct($defaultProduct->getId());
            $elasticSynchronizer->index($elasticTransformer->handle());
        }


        $io->success('Application has been successfully initialized.');
    }
}
