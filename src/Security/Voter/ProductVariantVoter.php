<?php

namespace App\Security\Voter;

use App\Entity\ProductVariant;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ProductVariantVoter
 * @package App\Security\Voter
 */
class ProductVariantVoter extends Voter
{
    /**
     * Voter Edit Action
     */
    CONST EDIT = 'EDIT';
    /**
     * Voter Delete Action
     */
    CONST DELETE = 'DELETE';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [$this::EDIT, $this::DELETE])
            && $subject instanceof ProductVariant;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if ( ! $user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case $this::EDIT:
                return $user->getId() == $subject->getProduct()->getUser()->getId();
                break;
            case $this::DELETE:
                return $user->getId() == $subject->getProduct()->getUser()->getId();
                break;
        }

        return false;
    }
}
